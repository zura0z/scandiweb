<?php
function validSKU($input)
{
    if ($_SERVER['REQUEST_METHOD'] === "POST") {
        require_once "./post.php";
        if (strlen($input) === 9) {
            return [true, "is-valid"];
        } else {
            return [false, "is-invalid"];
        }
    }
}
function validName($input)
{
    if ($_SERVER['REQUEST_METHOD'] === "POST") {
        if ($input !== "") {
            return [true, "is-valid"];
        } else {
            return [false, "is-invalid"];
        }
    }
}
function validPrice($input)
{
    if ($_SERVER['REQUEST_METHOD'] === "POST") {
        if ($input === "" || $input <= 0 || ($input[0] === "0" && $input[1] !== ".")) {
            return [false, "is-invalid"];
        } else {
            return [true, "is-valid"];
        }
    }
}
function validSwitcher($input)
{
    if ($_SERVER['REQUEST_METHOD'] === "POST") {
        if ($input !== "") {
            return [true, "is-valid"];
        } else {
            return [false, "is-invalid"];
        }
    }
}
