<?php
require_once "./connect.php";

foreach ($products as $i => $product) {
    if (isset($_POST[$product['id']])) {
        $statement = $pdo->prepare("DELETE FROM products WHERE id = :id");
        $statement->bindValue(':id', $product['id']);
        $statement->execute();
    }
}
header('Location: index.php');
