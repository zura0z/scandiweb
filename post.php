<?php
$errors = [];
$sku = '';
$name = '';
$price = '';
$switcher = '';
$weight = '';
$size = '';
$length = '';
$width = '';
$height = '';

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $sku = strtoupper($_POST['SKU']);
    $name = $_POST['name'];
    $price = $_POST['price'];
    $weight = $_POST['weight'];
    $size = $_POST['size'];
    $length = $_POST['length'];
    $width = $_POST['width'];
    $height = $_POST['height'];
    if (isset($_POST['switcher'])) {
        $switcher = $_POST['switcher'];
    }
    if (validPrice($height)[0]) {
        $dimensions = $height . "x" . $width . "x" . $length;
    }
    if (!validPrice($weight)[0]) {
        $weight = NULL;
    }
    if (!validPrice($size)[0]) {
        $size = NULL;
    }
    if (!validSKU($sku)[0]) {
        $errors[] = 'SKU';
    }
    if (!validName($name)[0]) {
        $errors[] = 'Name';
    }
    if (!validPrice($price)[0]) {
        $errors[] = 'Price';
    }
    if (!validSwitcher($switcher)[0]) {
        $errors[] = 'Type';
    }
    if (!validPrice($size)[0] && $switcher === "DVD") {
        $errors[] = 'Size';
    }
    if (!validPrice($weight)[0] && $switcher === "Book") {
        $errors[] = 'Weight';
    }
    if ((!validPrice($length)[0] || !validPrice($width)[0] || !validPrice($height)[0]) && $switcher === "Furniture") {
        $errors[] = 'Dimensions';
    }


    if (empty($errors)) {
        $statement = $pdo->prepare("INSERT INTO products ( SKU, Name, Price, Type_Switcher, Size, Weight, Dimensions, create_date)
        VALUES (:SKU, :Name, :Price, :Type_Switcher, :Size, :Weight, :Dimensions, :date)");
        $statement->bindValue(':SKU', $sku);
        $statement->bindValue(':Name', $name);
        $statement->bindValue(':Price', $price);
        $statement->bindValue(':Type_Switcher', $switcher);
        if ($switcher === "DVD") {
            $statement->bindValue(':Size', $size);
        } else $statement->bindValue(':Size', NULL);
        if ($switcher === "Book") {
            $statement->bindValue(':Weight', $weight);
        } else $statement->bindValue(':Weight', NULL);
        if ($switcher === "Furniture") {
            $statement->bindValue(':Dimensions', $dimensions);
        } else $statement->bindValue(':Dimensions', NULL);
        $statement->bindValue(':date', date('Y-m-d H:i:s', strtotime("+3 hour")));

        $statement->execute();
        header('Location: index.php');
    }
}
