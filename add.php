<style>
    <?php include_once './styles.css'; ?>
</style>
<?php
require_once "./header.php";
require_once "./connect.php";
require_once "./validations.php";
require_once "./post.php";
?>

<div id="back_btn"><a href="./index.php">Back</a></div>
<div class="container">
    <!-- HEADER -->
    <div class="add-product-header">
        <h1>Add Product</h1>
        <button form="form" type="submit" id="save-btn" class="btn btn-success btn-sm">Save</button>
    </div>
    <hr>
    <!-- /////////// -->
    
    <!-- FORM -->
    <form id="form" action="<?php echo $_SERVER["PHP_SELF"] ?>" method="post">
        <?php
        include_once "./inputs/sku_input.php";
        include_once "./inputs/name_input.php";
        include_once "./inputs/price_input.php";
        include_once "./inputs/switcher_input.php";
        include_once "./inputs/size_input.php";
        include_once "./inputs/weight_input.php";
        include_once "./inputs/dimensions_input.php";
        ?>
    </form>
    <!-- /////////// -->
</div>

<!-- FOOTER / TYPE SWITCHER SCRIPT-->
<?php
require_once "./type_switcher.php";
require_once "./footer.php";
?>
<!-- /////////////////////////// -->