<div id="size_input" class="col-md-6 mb-3 hide type">
    <input type="number" step="0.01" class="form-control <?php echo validPrice($size)[1] ?>" placeholder="Size" name="size" value="<?php echo $size ?>">
    <div class="valid-feedback">
        Looks good!
    </div>
    <div class="invalid-feedback">
        Please provide valid Size.
    </div>
    <small>Please provide DVD size in MB</small>
</div>