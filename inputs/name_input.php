<div class="col-md-6 mb-3">
    <input type="text" class="form-control <?php echo validName($name)[1] ?>" placeholder="Product Name" name="name" value="<?php echo $name ?>">
    <div class="valid-feedback">
        Looks good!
    </div>
    <div class="invalid-feedback">
        Please provide valid Name.
    </div>
</div>