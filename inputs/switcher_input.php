<div class="col-md-6 mb-3">
    <select id="select" class="custom-select <?php echo validSwitcher($switcher)[1] ?>" name="switcher">
        <option <?php if ($switcher === '') echo "selected='true'" ?> disabled value="">Type Switcher</option>
        <option id="DVD" <?php if ($switcher === "DVD") echo "selected='true'" ?> value="DVD">DVD-disc</option>
        <option id="Book" <?php if ($switcher === "Book") echo "selected='true'" ?> value="Book">Book</option>
        <option id="Furniture" <?php if ($switcher === "Furniture") echo "selected='true'" ?> value="Furniture">Furniture</option>
    </select>
    <div class="valid-feedback">
        Looks good!
    </div>
    <div class="invalid-feedback">
        Please select a Type.
    </div>
</div>