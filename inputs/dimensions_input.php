<div id="dimensions_input" class="hide type col-md-6 mb-3">
    <div id="dimensions_input-display">
        <div class="col-xs-2">
            <input type="number" class="form-control mr-4 <?php echo validPrice($height)[1] ?>" placeholder="Height" name="height" value="<?php echo $height ?>">
            <div class="valid-feedback">
                Looks good!
            </div>
            <div class="invalid-feedback">
                Please provide valid Height.
            </div>
        </div>
        <div class="col-xs-2">
            <input type="number" class="form-control mr-4 <?php echo validPrice($width)[1] ?>" placeholder="Width" name="width" value="<?php echo $width ?>">
            <div class="valid-feedback">
                Looks good!
            </div>
            <div class="invalid-feedback">
                Please provide valid Width.
            </div>
        </div>
        <div class="col-xs-2">
            <input type="number" class="form-control mr-4 <?php echo validPrice($length)[1] ?>" placeholder="Length" name="length" value="<?php echo $length ?>">
            <div class="valid-feedback">
                Looks good!
            </div>
            <div class="invalid-feedback">
                Please provide valid Lenght.
            </div>
        </div>
    </div>
    <small>Please provide dimensions in HxWxL format</small>
</div>