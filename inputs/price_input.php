<div class="col-md-6 mb-3">
    <input type="number" step="0.01" class="form-control <?php echo validPrice($price)[1] ?>" placeholder="Price" name="price" value="<?php echo $price ?>">
    <div class="valid-feedback">
        Looks good!
    </div>
    <div class="invalid-feedback">
        Please provide valid Price.
    </div>
</div>