<div class="col-md-6 mb-3">
    <input type="text" class="form-control <?php echo validSKU($sku)[1] ?>" placeholder="SKU" name="SKU" value="<?php echo $sku ?>">
    <div class="valid-feedback">
        Looks good!
    </div>
    <div class="invalid-feedback">
        SKU should be 9 characters.
    </div>
</div>