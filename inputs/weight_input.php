<div id="weight_input" class="col-md-6 mb-3 hide type">
    <input type="number" step="0.01" class="form-control <?php echo validPrice($weight)[1] ?>" placeholder="Weight" name="weight" value="<?php echo $weight ?>">
    <div class="valid-feedback">
        Looks good!
    </div>
    <div class="invalid-feedback">
        Please provide valid Weight.
    </div>
    <small>Please provide Book weight in KG</small>
</div>