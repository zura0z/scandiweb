<style>
    <?php include_once './styles.css'; ?>
</style>
<?php
require_once "./header.php";
require_once "./connect.php";
?>

<div class="container">
    <div class="add-product-header">
        <h1>Product List</h1>
        <div>
            <a href="./add.php" class="btn btn-success">Add Product</a>
            <button form="checkForm" type="submit" class="btn btn-outline-danger">Delete</button>
        </div>
    </div>
    <hr>
    <form id="checkForm" method="post" action="delete.php" style="display: inline-block">
        <div class="product-container">
            <?php foreach ($products as $i => $product) { ?>
                <div class="product">
                    <input class="checkbox" type="checkbox" value="<?php echo $product["id"] ?>" name="<?php echo $product["id"] ?>">
                    <h5>SKU: <?php echo $product['SKU'] ?></h5>
                    <h5>Name: <?php echo $product['Name'] ?></h5>
                    <h5>Price: <?php echo $product['Price'] . " $" ?></h5>
                    <h5>
                        <?php
                        if (strlen($product['Weight'])) echo "Weight: " . $product['Weight'] . " KG";
                        if (strlen($product['Size'])) echo "Size: " . $product['Size'] . " MB";
                        if (isset($product['Dimensions'])) echo "Dimensions: " . $product['Dimensions'];
                        ?>
                    </h5>
                </div>
            <?php } ?>
        </div>
    </form>
</div>

<?php
require_once "./footer.php";
?>