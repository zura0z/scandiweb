<script>
    const switcher = () => {
        if (document.getElementById("DVD").selected) {
            document.getElementById("size_input").classList.remove("hide");
        } else document.getElementById("size_input").classList.add("hide");
        if (document.getElementById("Book").selected) {
            document.getElementById("weight_input").classList.remove("hide");
        } else document.getElementById("weight_input").classList.add("hide");
        if (document.getElementById("Furniture").selected) {
            document.getElementById("dimensions_input").classList.remove("hide");
        } else document.getElementById("dimensions_input").classList.add("hide");
    }
    document.getElementById("select").onchange = () => switcher();
    document.getElementById("body").onload = () => switcher();
</script>